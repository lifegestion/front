import { Injectable } from '@angular/core';
import { Todo } from '../../interfaces/todoInterface';
import { Observable, of } from 'rxjs';
import TodoList from '../../contsants/todoConst';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  constructor() {}

  getTodoList(): Observable<Todo[]> {
    const todoList = of(TodoList);
    return todoList;
  }

  updateTodo(data: Todo): Todo[] {
    const ubtatedTodo = TodoList.map((todo) =>
      todo.id === data.id ? { ...todo, ...data } : todo
    );
    return ubtatedTodo;
  }

  createTodo(data: Todo): Todo[] {
    TodoList.push(data);
    const todoList = TodoList;
    return todoList;
  }

  deleteTodo(id: number): Todo[] {
    const deletedTodo = TodoList.filter((todo) => todo.id !== id);
    return deletedTodo;
  }
  
}
