import { Todo } from "../interfaces/todoInterface";

let TodoList: Todo[] = [
  {
    id: 1,
    name: 'Todo 1',
    description: 'Description 1',
    status: true,
  },
  {
    id: 2,
    name: 'Todo 2',
    description: 'Description 2',
    status: false,
  },
  {
    id: 3,
    name: 'Todo 3',
    description: 'Description 3',
    status: true,
  },
  {
    id: 4,
    name: 'Todo 4',
    description: 'Description 4',
    status: false,
  },
];

export default TodoList;
