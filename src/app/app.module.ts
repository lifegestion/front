import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { TodoListComponent } from './features/todo-list/todo-list.component';
import { AppComponent } from './app.component';
@NgModule({
  imports: [FormsModule, BrowserModule],
})
export class AppModule {}
