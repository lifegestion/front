import { Component, Input, OnInit } from '@angular/core';
import { RouterLink, Router, NavigationEnd } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-nav',
  standalone: true,
  imports: [RouterLink, FontAwesomeModule],
  templateUrl: './nav.component.html',
  styleUrl: './nav.component.sass',
})
export class NavComponent {
  portfolioUrl = 'https://yanissmarelle.fr/'
  faUser = faUser;

  @Input() currentUrl: string = '';

  constructor() {}


}
