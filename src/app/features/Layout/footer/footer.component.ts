import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { RouterLink } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  faUser,
  faHouse,
  faList,
  faWallet,
  faDumbbell,
  faBasketShopping,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [RouterLink, FontAwesomeModule],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.sass',
})

export class FooterComponent implements OnChanges {
  faUser = faUser;
  faHouse = faHouse;
  faList = faList;
  faWallet = faWallet;
  faDumbbell = faDumbbell;
  faBasketShopping = faBasketShopping;

  @Input() currentUrl: string = '';
  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['currentUrl']) {
      this.currentUrl = changes['currentUrl'].currentValue;
    }
  }
}
