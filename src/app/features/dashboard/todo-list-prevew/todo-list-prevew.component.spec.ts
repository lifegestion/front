import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListPrevewComponent } from './todo-list-prevew.component';

describe('TodoListPrevewComponent', () => {
  let component: TodoListPrevewComponent;
  let fixture: ComponentFixture<TodoListPrevewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TodoListPrevewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TodoListPrevewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
