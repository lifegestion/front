import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SportPrevewComponent } from './sport-prevew.component';

describe('SportPrevewComponent', () => {
  let component: SportPrevewComponent;
  let fixture: ComponentFixture<SportPrevewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SportPrevewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SportPrevewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
