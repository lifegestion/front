import { NgIf } from '@angular/common';
import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'app-wallet-prevew',
  standalone: true,
  imports: [NgIf, RouterLink],
  templateUrl: './wallet-prevew.component.html',
  styleUrl: './wallet-prevew.component.sass',
})
export class WalletPrevewComponent {
  amountState: boolean = false;
  constructor() {}
  test(item: string) {
    for (let i = 0; i < item.length; i++) {
      if (item[i] === '-') {
        this.amountState = false;
      }
    }
    this.amountState = true;
  }
}
