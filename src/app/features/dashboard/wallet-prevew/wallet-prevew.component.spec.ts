import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletPrevewComponent } from './wallet-prevew.component';

describe('WalletPrevewComponent', () => {
  let component: WalletPrevewComponent;
  let fixture: ComponentFixture<WalletPrevewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [WalletPrevewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(WalletPrevewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
