import { Component, OnInit } from '@angular/core';
import { TodoListPrevewComponent } from './todo-list-prevew/todo-list-prevew.component';
import { SportPrevewComponent } from './sport-prevew/sport-prevew.component';
import { WalletPrevewComponent } from './wallet-prevew/wallet-prevew.component';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    WalletPrevewComponent,
    TodoListPrevewComponent,
    SportPrevewComponent,
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.sass',
})
export class DashboardComponent {
  constructor() {}

 
}
