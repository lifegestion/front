import { Component, Input } from '@angular/core';
import { Todo } from '../../../interfaces/todoInterface';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faCheck, faXmark } from '@fortawesome/free-solid-svg-icons';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-todo-card',
  standalone: true,
  imports: [FontAwesomeModule, NgIf],
  templateUrl: './todo-card.component.html',
  styleUrl: './todo-card.component.sass',
})
export class TodoCardComponent {
  @Input() todo?: Todo;
  faCheck = faCheck;
  faXmark = faXmark;
  
}
