import {
  Component,
  Input,
  OnChanges,
  SimpleChanges,
  Output,
  EventEmitter,
} from '@angular/core';
import { Todo } from '../../../../interfaces/todoInterface';
import { NgIf } from '@angular/common';
import { TodoService } from '../../../../services/todo/todo.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { faTrashCan } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [NgIf, FormsModule, ReactiveFormsModule, FontAwesomeModule],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.sass',
})
export class TodoComponent implements OnChanges {
  @Input() todo?: Todo;

  @Output() updateTodoList = new EventEmitter<Todo[]>();
  @Output() closeTodo = new EventEmitter<boolean>();

  form: FormGroup;
  faTrashCan = faTrashCan;
  
  constructor(
    private readonly todoService: TodoService,
    private readonly fb: FormBuilder
  ) {
    this.form = this.buildForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['todo'] && this.todo) {
      this.initForm();
    } else {
      this.form.reset();
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      name: [null],
      description: [null],
      status: [null],
    });
  }

  initForm() {
    console.log();

    if (this.todo) {
      this.form.setValue({
        name: this.todo.name,
        description: this.todo.description,
        status: this.todo.status,
      });
    }
  }

  get nameFormCtrl(): FormControl {
    return this.form.get('name') as FormControl;
  }

  get descriptionFormCtrl(): FormControl {
    return this.form.get('description') as FormControl;
  }

  get statusFormCtrl(): FormControl {
    return this.form.get('status') as FormControl;
  }

  updateTodo(): Todo[] {
    if (this.todo) {
      this.todo.name = this.nameFormCtrl.value;
      this.todo.description = this.descriptionFormCtrl.value;
      this.todo.status = this.statusFormCtrl.value;
    }
    this.closeTodo.emit(false);
    return this.todoService.updateTodo(this.todo!);
  }

  createTodo(): Todo[] {
    const newTodo: Todo = {
      id: 5,
      name: this.nameFormCtrl.value,
      description: this.descriptionFormCtrl.value,
      status: this.statusFormCtrl.value,
    };
    this.closeTodo.emit(false);
    return this.todoService.createTodo(newTodo);
  }

  deleteTodo(): Todo[] {
    if (!this.todo) {
      return [];
    }
    this.updateTodoList.emit(this.todoService.deleteTodo(this.todo.id));
    this.closeTodo.emit(false);
    return this.todoService.deleteTodo(this.todo.id);
  }
}
