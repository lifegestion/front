import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Todo } from '../../interfaces/todoInterface';
import { NgFor, NgIf } from '@angular/common';
import { TodoComponent } from './components/todo/todo.component';
import { TodoService } from '../../services/todo/todo.service';
import { TodoCardComponent } from './todo-card/todo-card.component';

@Component({
  selector: 'app-todo-list',
  standalone: true,
  imports: [NgFor, NgIf, TodoComponent, TodoCardComponent],
  templateUrl: './todo-list.component.html',
  styleUrl: './todo-list.component.sass',
})
export class TodoListComponent implements OnInit {
  constructor(
    private todoService: TodoService
  ) {}

  todo: Todo[] = [];
  openTodo: boolean = false;
  selectedTodo: Todo | undefined;

  ngOnInit(): void {
    this.getTodoList();
  }

  getTodoList(): void {
    this.todoService.getTodoList().subscribe((todo) => (this.todo = todo));
  }

  onCreateTodo(): void {
    this.openTodo = true;
    this.selectedTodo = undefined;
  }

  onSelect(todo: Todo): void {
    this.selectedTodo = todo;
    this.openTodo = true;
  }

  newTodoList(todoList: Todo[]): void {
    this.todo = todoList;
  }

  onCloseTodo(event: boolean): void {
    this.openTodo = event;
  }
}
