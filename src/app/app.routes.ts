import { Routes } from '@angular/router';
import { TodoListComponent } from './features/todo-list/todo-list.component';
import { WalletComponent } from './features/wallet/wallet.component';
import { ShoppingComponent } from './features/shopping/shopping.component';
import { DashboardComponent } from './features/dashboard/dashboard.component';

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
  {
    path: 'todolist',
    component: TodoListComponent,
  },
  {
    path: 'wallet',
    component: WalletComponent,
  },
  {
    path: 'shopping',
    component: ShoppingComponent,
  },
];
